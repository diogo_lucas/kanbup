# Kanbup

## *A Kanban mashup app*

Kanbup allows you to set up kanban boards from different sources -- you can use it to consolidate your tasks from different systems in one single place.

## To-do
* Use lockit for user mgmt
* Add multiple OAuth integration, for systems that will contain to-do cards