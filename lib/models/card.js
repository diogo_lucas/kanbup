'use strict';

var 
	mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
/**
 * Thing Schema
 */
var CardSchema = new Schema({
  title: String,
  description: String,
  done: Boolean,
  starred: Boolean,
  owner_id: Schema.ObjectId
});

CardSchema
	.path('title')
	.validate(function (title) {
		if (title) {
			return !(title === '');
		}
		return false;
	}, 'Title is mandatory');

CardSchema
	.path('owner_id')
	.validate(function (owner_id) {
		return owner_id;
	}, 'Owner id is mandatory');


mongoose.model('Card', CardSchema);
