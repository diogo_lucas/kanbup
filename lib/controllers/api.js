'use strict';

var mongoose = require('mongoose'),
	  Card = mongoose.model('Card'),
    Thing = mongoose.model('Thing');

/**
 * Get awesome things
 */

exports.awesomeThings = function(req, res) {
  return Thing.find(function (err, things) {
    if (!err) {
      return res.json(things);
    } else {
      return res.send(err);
    }
  });
};
/*
exports.cards = function(req, res) {
  return Card.find(function (err, cards) {
    if (!err) {
      return res.json(cards);
    } else {
      return res.send(err);
    }
  });
};
*/