'use strict';

var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Card = mongoose.model('Card'),
  Thing = mongoose.model('Thing'),
  user_id = mongoose.Types.ObjectId();

/**
 * Populate database with sample application data
 */

module.exports = function(done) {
  var doneCount = 0;

  function checkDone() {
    if (++doneCount == 3) {
      done();
    }
  }

  //Clear old things, then add things in
  Thing.find({}).remove(function() {
    Thing.create({
      name : 'HTML5 Boilerplate',
      info : 'HTML5 Boilerplate is a professional front-end template for building fast, robust, and adaptable web apps or sites.',
      awesomeness: 10
    }, {
      name : 'AngularJS',
      info : 'AngularJS is a toolset for building the framework most suited to your application development.',
      awesomeness: 10
    }, {
      name : 'Karma',
      info : 'Spectacular Test Runner for JavaScript.',
      awesomeness: 10
    }, {
      name : 'Express',
      info : 'Flexible and minimalist web application framework for node.js.',
      awesomeness: 10
    }, {
      name : 'MongoDB + Mongoose',
      info : 'An excellent document database. Combined with Mongoose to simplify adding validation and business logic.',
      awesomeness: 10
    }, function() {
      checkDone();
    });
  });

  // Clear old users, then add a default user
  User.find({}).remove(function() {
    User.create({
      _id: user_id,
      provider: 'local',
      name: 'Test User',
      email: 'test@test.com',
      password: 'test'
    }, function() {
      checkDone();
    });
  });

  // Clear old cards, then add a default card
  Card.find({}).remove(function() {
    Card.create({
      title: 'Card 1',
      description: 'Silly text for card 1',
      done: false,
      starred: true,
      owner_id: user_id
    }, {
      title: 'Card 2',
      description: 'Silly text for card 2',
      done: true,
      starred: false,
      owner_id: user_id
    }, function() {
      checkDone();
    });
  });
};