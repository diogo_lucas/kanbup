'use strict';

angular.module('kanbupApp')
  .factory('Session', function ($resource) {
    return $resource('/api/session/');
  });
