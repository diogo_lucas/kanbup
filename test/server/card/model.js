'use strict';

console.log(__dirname);

var 
    requireHelper = require('../require_helper'),
    should = require('should'),
    mongoose = require('mongoose'),
    crd = requireHelper('models/card.js'),
    srv = requireHelper('server.js'),
    Card = mongoose.model('Card');


var card;

describe('Card Model', function() {
  beforeEach(function(done) {
    card = new Card({
      title: 'Fake Card',
      description: 'Fake description',
      done: true,
      starred: true,
      owner_id: new mongoose.Types.ObjectId()
    });

    Card.remove().exec();
    done();
  });

  afterEach(function(done) {
    Card.remove().exec();
    done();
  });

  it('should begin with no cards', function(done) {
    Card.find({}, function(err, cards) {
      cards.should.have.length(0);
      done();
    });
  });

  it('should fail when saving without a title', function(done) {
    card.title = null;
    card.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without an owner', function(done) {
    card.owner_id = null;
    card.save(function(err) {
      should.exist(err);
      done();
    });
  });
});
